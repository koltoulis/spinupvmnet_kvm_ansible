---

- name: Generate User Data For Router Device
  shell:
    cmd: |
      cat > "{{ working_dir }}{{ item.value.name }}/user-data" <<  EOF
      #cloud-config
      users:
        - name: ansible
          sudo:  ALL=(ALL) NOPASSWD:ALL
          ssh_authorized_keys:
            - $(cat {{ working_dir }}creds/ansible.pub.gpg | gpg --batch --passphrase {{ gpg_passphrase }} )
        - name: recovery
          sudo:  ALL=(ALL) ALL
          passwd: $(openssl passwd -1 -salt SaltSalt {{ recovery_pass }})
          lock_passwd: false
      runcmd:
      - sed -i s/^SELINUX=.*$/SELINUX=disabled/ /etc/selinux/config
      - setenforce 0
      - [ systemctl, enable, iptables.service ]
      - [ systemctl, start, iptables.service ]
      - [ systemctl, enable, dnsmasq.service ]
      - [ systemctl, start, dnsmasq.service ]
      - sysctl -w net.ipv4.ip_forward=1
      - iptables -D FORWARD 1
      - iptables -A FORWARD -j ACCEPT
      {% for arg in range( item.value.networks | length ) %}
      {% for network in item.value.networks[arg].values() %}
      {% if network['name'] is search("nat") %}
      - iptables -t nat -I POSTROUTING -o eth{{ arg }} -j MASQUERADE
      {% endif %}
      {% if network['dhcp'] == 'active' %}
      - iptables -I INPUT -i eth{{ arg }} -p udp --dport 67 -j ACCEPT
      - iptables -I INPUT -i eth{{ arg }} -p udp --dport 53 -j ACCEPT
      - iptables -I INPUT -i eth{{ arg }} -p tcp --dport 53 -j ACCEPT
      {% endif %}
      {% endfor %}
      {% endfor %}
      - iptables -I INPUT -m tcp -p tcp --dport 2379 -j ACCEPT
      - cp /etc/sysconfig/iptables ~
      - iptables-save > /etc/sysconfig/iptables
      - echo "net.ipv4.ip_forward = 1" > /etc/sysctl.conf
      - sysctl -p /etc/sysctl.conf
      - service network restart
      {% for arg in range( item.value.networks | length ) %}
      {% for network in item.value.networks[arg].values() %}
      {% if network['dhcp'] == 'active' %}
      - echo "interface=eth{{ arg }}" >> /etc/dnsmasq.conf
      - echo "dhcp-range=eth{{ arg }},{{  network['net'].split(".")[0:3]|join(".") }}.5,{{  network['net'].split(".")[0:3]|join(".") }}.30,1h" >> /etc/dnsmasq.conf
      {% endif %}
      {% endfor %}
      {% endfor %}
      - [ systemctl, restart, dnsmasq.service ]
      - echo 'nameserver 192.168.1.1' > /etc/resolv.conf
      - sed -i /ETCD_LISTEN_CLIENT_URLS=.*$/s/localhost/0.0.0.0/ /etc/etcd/etcd.conf
      - [ systemctl, start, etcd.service ]
      - [ systemctl, enable, etcd.service ]
      - git clone 'https://{{ repo_user }}:{{ repo_pass }}@bitbucket.org/koltoulis/creds.git'
      - cat creds/ansible_priv.gpg | gpg --batch --passphrase {{ gpg_passphrase }} > creds/ansible_priv
      - cp creds/ansible_priv /home/ansible/.ssh/
      - chmod 600 /home/ansible/.ssh/ansible_priv
      - rm -rf creds/
      - git clone 'https://{{ repo_user }}:{{ repo_pass }}@bitbucket.org/koltoulis/ansible_dynamic_inventory.git'
      - sleep 240
      - python ansible_dynamic_inventory/parse_etcd_hosts.py
      - chown -R ansible:ansible /home/ansible
      packages:
        - ansible
        - etcd
        - git
        - iptables
        - iptables-utils
        - iptables-services
        - dnsmasq
      EOF

- name: Generate Meta Data For Router Device
  shell:
    cmd: |
      cat > "{{ working_dir }}{{ item.value.name }}/meta-data" <<  EOF
      instance-id: {{item.value.name}}.local;
      local-hostname: {{ item.value.name }};
      EOF

- name: Generate Network Data For Router Device
  shell:
    cmd: |
      cat > "{{ working_dir }}{{ item.value.name }}/network-config" << EOF
      version: 1
      config:
      {% for arg in range( item.value.networks | length ) %}
      {% for network in item.value.networks[arg].values() %}
      {% if network['dhcp'] == 'True' %}
      - type: physical
        name: eth{{arg}}
        subnets:
          - type: dhcp
      {% endif %}
      {% if network['dhcp'] == 'False' %}
      - type: physical
        name: eth{{arg}}
        subnets:
          - type: static
            address: {{ network['ip'] }}
            netmask: {{ network['netmask'] }}
      {% if network['gateway'] != 'none' %}
            gateway: {{ network['gateway'] }}
      {% endif %}{% endif %}{% if network['dhcp'] == 'active' %}
      - type: physical
        name: eth{{arg}}
        subnets:
          - type: static
            address: {{ network['ip'] }}
            netmask: {{ network['netmask'] }}
      {% endif %}
      {% endfor %}
      {% endfor %}
      - type: nameserver
        address:
          - 192.168.1.1
      EOF